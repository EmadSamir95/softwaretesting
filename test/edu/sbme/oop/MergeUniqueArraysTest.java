/*
author : Emad Samir
*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sbme.oop;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class MergeUniqueArraysTest {
    
    public MergeUniqueArraysTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of mergeArrays method, of class MergeUniqueArrays.
     */
    @Test
    public void testMergeArrays() {
       int [] Arr1 = new int [] {1,3,5,7};
       int [] Arr2 = new int [] {2,4,6,8};
       int [] Sorted = new int [] {1,2,3,4,5,6,7,8};
        assertArrayEquals(Sorted, MergeUniqueArrays.mergeArrays(Arr1, Arr2));
    }
    
    @Test
     public void testMergeArrays2() {
       int [] Arr1 = new int [] {1,3,5,7,9};
       int [] Arr2 = new int [] {2,4,6,8};
       int [] Sorted = new int [] {1,2,3,4,5,6,7,8,9};
        assertArrayEquals(Sorted, MergeUniqueArrays.mergeArrays(Arr1, Arr2));
    }
     
    @Test
     public void testMergeArrays3() {
       int [] Arr1 = new int [] {};
       int [] Arr2 = new int [] {2,4,6,8};
       int [] Sorted = new int [] {2,4,6,8};
        assertArrayEquals(Sorted, MergeUniqueArrays.mergeArrays(Arr1, Arr2));
    }
     
    @Test
     public void testMergeArrays4() {
       int [] Arr1 = new int [] {1,3,5,7};
       int [] Arr2 = new int [] {};
       int [] Sorted = new int [] {1,3,5,7};
        assertArrayEquals(Sorted, MergeUniqueArrays.mergeArrays(Arr1, Arr2));
    }
     
    @Test
     public void testMergeArrays5() {
       int [] Arr1 = new int [] {};
       int [] Arr2 = new int [] {};
       int [] Sorted = new int [] {};
        assertTrue(MergeUniqueArrays.mergeArrays(Arr1, Arr2)==null);
     }
    
}
