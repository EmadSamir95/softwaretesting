/*
author : Emad Samir
*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.sbme.oop;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class PatientDaoImplTest {
    
    public PatientDaoImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insert_patient method, of class PatientDaoImpl.
     */
    @Test
    public void testInsert_patient() throws PatientDaoException{
        Patient patient = new Patient("Ali", "Ahmed");
        patient.setPhNum("2222");
        patient.setSSN("234234");
       PatientDaoImpl e = new PatientDaoImpl();
       e.insert_patient(patient);
        
    }
    
}
