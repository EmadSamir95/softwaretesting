/*
author : Emad Samir
*/

package edu.sbme.oop;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class PatientTest {
    
    public PatientTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setSSN method, of class Patient.
     */
    
    @Test
    public void constructorTest(){
       Patient p = new Patient("Ahmed", "Ali");
       p.setPhNum("2323");
       p.setSSN("2345345345");
       assertEquals(p.getfname(),"Ahmed");
       assertEquals(p.getlname(), "Ali");
       assertEquals(p.getSSN(), "2345345345");
       assertEquals(p.getPhNum(), "2323");
        
    }
    
    
    
    
}
